/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
import Immutable from 'immutable';

class Repository extends React.Component{
  constructor(props){
    super(props);
    this.radius = 3;
  }

  calculateLayout(parentHash, children, layouts){
    const parentLayout = layouts.get(parentHash);
    const childHorizontalRangeWidth = (parentLayout.horizontalRange.to - parentLayout.horizontalRange.from)/(children.length);

    return children.reduce((soFar, child, index) => {
      const childLayout = {
        horizontalRange: {
          from: parentLayout.horizontalRange.from + childHorizontalRangeWidth * (index),
          to: parentLayout.horizontalRange.from + childHorizontalRangeWidth * (index+1)
        },
        position: {
          y: parentLayout.position.y + this.radius*3.5,
          x: parentLayout.horizontalRange.from + childHorizontalRangeWidth * (index+0.5)
        },
        labelPosition: (index < (children.length - 1) / 2) ? 'left' : 'right'
      };
      return this.calculateLayout(child, this.props.tree.get(child).children, soFar.set(child, childLayout));
    }, layouts);
  }

  drawEdges(layout){
    return layout.map((nodeLayout, nodeHash) => {
      const parents = this.props.tree.get(nodeHash).parents;
      return parents.map(parent => {
        const parentLayout = layout.get(parent);
        return <line strokeWidth={ this.radius / 20 }
                     key={`line-${parent}-${nodeHash}`}
                     x1={parentLayout.position.x} y1={parentLayout.position.y}
                     x2={nodeLayout.position.x}
                     y2={nodeLayout.position.y} />
      });
    })
    .flatten(true);
  }

  drawNodes(layout){
    return layout.map((nodeLayout, commitHash) => {
      return <circle cx={nodeLayout.position.x} cy={nodeLayout.position.y} r={this.radius}
                     className={`commit-${commitHash}-${nodeLayout.position.x}-${nodeLayout.position.y}`}
                     key={`commit-${commitHash}`} />;
    });
  }

  drawHead(layout){
    const headPosition = layout.get(this.props.head);
    let xPosition = headPosition.position.x + this.radius+1,
        textAnchor = 'start';
    if(headPosition.labelPosition === 'left'){
        xPosition = headPosition.position.x - (this.radius+1);
        textAnchor = 'end';
    }
    return <text key="head" id="head" x={xPosition} y={headPosition.position.y - this.radius} textAnchor={textAnchor} fontSize={this.radius * 2/3}>HEAD</text>
  }

  drawBranches(layout){
    return this.props.branches.reduce((soFar, commit, branch) => {
      const nodePosition = layout.get(commit);
      const fontSize = this.radius * 2/3;
      const currentBranchClass = (this.props.currentBranch === branch) ? 'is-current' : '';
      let xPosition = nodePosition.position.x + this.radius+1,
          textAnchor = 'start';
      if(nodePosition.labelPosition === 'left'){
          xPosition = nodePosition.position.x - (this.radius+1);
          textAnchor = 'end';
      }
      return soFar.push(
        <text className={`branch ${branch} ${currentBranchClass}`}
              key={`branch-${branch}`}
              x={xPosition}
              textAnchor={textAnchor}
              // Y position of associated node
              // - radius (to top-align with the node)
              // + height of a branch name * (number of branches listed above it + height of the HEAD label)
              y={nodePosition.position.y - this.radius + fontSize * (soFar.size +  1)}
              fontSize={fontSize}>{branch}
        </text>);
    }, Immutable.List());
  }

  render() {
    if(typeof(this.props.tree) === 'undefined' || typeof(this.props.root) === 'undefined'){
      return false;
    }

    const rootLayout = Immutable.Map({
      // We take 70% of the width for drawing the nodes.
      // This leaves 15% for labels on either side.
      [this.props.root]: {
        position: { x: 50, y: 2 * this.radius },
        horizontalRange: { from: 15, to: 85 }
      }
    });

    const rootChildren = this.props.tree.get(this.props.root).children;
    const layout = this.calculateLayout(this.props.root, rootChildren, rootLayout);
    return <g>
      <ReactCSSTransitionGroup transitionName="trees" component="g" transitionEnterTimeout={300} transitionLeaveTimeout={300}>
        { this.drawEdges(layout) }
        { this.drawNodes(layout) }
        { this.drawHead(layout) }
        { this.drawBranches(layout) }
      </ReactCSSTransitionGroup>
    </g>;
  }
}
Repository.propTypes = {
  tree: React.PropTypes.instanceOf(Immutable.Map).isRequired,
  root: React.PropTypes.string.isRequired,
  head: React.PropTypes.string
};

module.exports = Repository;
