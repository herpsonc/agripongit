/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var CommitToOldBranch = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>The <em>current</em> branch is still <code>master</code> though. You can see what this means when we make some more changes and create another commit:</p>
      <Highlight className="bash">git commit</Highlight>
      <p>Whereas the <code>master</code> branch moved along with <code>HEAD</code>, <code>myBranch</code> is still pointing to the previous commit. We will see why this is useful in a bit.</p>
    </div>;
  }
});

module.exports = CommitToOldBranch;
