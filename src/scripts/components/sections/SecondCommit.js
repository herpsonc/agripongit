/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var SecondCommit = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Nous "stagons" le résumé <code>git add résumé.txt</code>puis créons un nouveau commit:</p>
      <Highlight className="bash">git commit</Highlight>
      <p>Comme vous pouvez le voir, ce nouveau commit est connecté au précédent (son <em>parent</em>). Vous pouvez voir un commit comme contenant la liste des modifications nécessaires pour que git puisse restaurer les fichiers dans leur état précédent.</p>
      <p>Considérons maintenant le label <code>HEAD</code> inscrit à coté du nouveau commit. <code>HEAD</code> peut être vu comme un pointeur: lorsque un nouveau commit est fait, il va être ajouté sous le noeud étiqueté <code>HEAD</code></p>
      <p>A titre d'exemple, imaginons que nous avons fait une erreur dans le nom d'une des personnes présente dans les remerciements. Après avoir effecuté la correction nous "stagons" le fichier <code>git add remerciements.txt</code> avant de le commiter:</p>
    </div>;
  }
});

module.exports = SecondCommit;
