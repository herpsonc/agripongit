/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var FirstCommit = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Voil à quoi l'historique local de Git's ressemble après notre premier commit</p>
      <Highlight className="bash">git commit</Highlight>
      <p>Le cercle représente le premier commit - Ignorez pour le moment les labels qui s'affichent à coté.</p>
      <p>Si, ultérieurement nous souhaitons revenir à cet état dans l'histoire du projet,  nous nous retrouverions avec le fichier  <code>remerciements.txt</code> dans son état actuel.</p>
      <p>Pourquoi ne pas avoir inclu <code>résumé.txt</code> dans ce premier commit ? C'était possible, mais, traditionnellement, une règle de bonne conduite avec git est de ne commiter ensemble que des modifications que nous serions susceptibles d'annuler ensemble. Dans le cas de notre livre, si à l'avenir nous souhaitons en modifier la structure, nous serions amener à changer le résumé sans pour autant vouloir supprimer les remerciements.</p>
      <p>Par conséquent, Git nous recommende d'envoyer <code>résumé.txt</code> dans un commit différent.</p>
    </div>;
  }
});

module.exports = FirstCommit;
