/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var PushMerge = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Since our local branch is now ahead of the remote's, we can push it back to the remote.</p>
      <Highlight className="bash">git push publisher master</Highlight>
      <p>Our copy editor can now fetch the latest version that includes both the corrections he made, and the changes we made.</p>
    </div>;
  }
});

module.exports = PushMerge;
