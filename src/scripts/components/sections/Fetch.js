/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var Fetch = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>To check whether this is the case, let's ask what has happened since the last time we contacted it (i.e. during our push):</p>
      <Highlight className="bash">git fetch publisher</Highlight>
      <p>And indeed, a new commit was added to <code>publisher</code>'s <code>master</code> branch. As you can see, while we weren't paying attention, our version of the branch sneakily diverged from the one at our publisher! This means we would not have been able to push the new version of our branch to our remote. It can be solved by merging the remote's work back into our own, making the history of our remote branch part of our local branch's history.</p>
    </div>;
  }
});

module.exports = Fetch;
