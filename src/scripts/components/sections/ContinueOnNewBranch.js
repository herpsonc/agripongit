/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var ContinueOnNewBranch = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Since we are still on <code>myBranch</code>, new commits will move it along with <code>HEAD</code>:</p>
      <Highlight className="bash">git commit</Highlight>
      <p>Branches are useful because it allows us to work on multiple things in parallel, without those things interfering with each other. For example, you could start work on a new chapter in a branch that only contains changes to that chapter. Now, when your editor is bugging you to <q>finally submit a manuscript already darnit!</q> (hypothetically), you can simply switch back to the <code>master</code> branch, perhaps fix up a few typos, and then send it to your editor. All this without having to include a half-finished chapter that wasn't that interesting anyway.</p>
      <p>Anyway, let's say we <em>did</em> finish that chapter. That's nice and all, but now it would be nice if we wouldn't have to redo the small changes we performed earlier in the <code>master</code> branch.</p>
    </div>;
  }
});

module.exports = ContinueOnNewBranch;
