/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var Init = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Imaginons que nous souhaitons démarrer un nouveau projet que nous voulons versionner (sauvegarder) grâce à Git. Ce projet pourrait être n'importe quoi, mais dans le cadre de ce tutoriel faisons l'hypothese que nous écrivons un livre. La premiere chose à faire est d'informer git (préalablement installé sur votre machine) de notre volonté. Dans un terminal, on se place dans le répertoire qui va contenir notre livre et nous informons git que ce répertoire doit devenir un répertoire Git</p>
      <Highlight className="bash">git init</Highlight>
      <p>Git crée alors un répertoire caché dans votre répertoire appelé <code>.git</code> — Tout ce que Git connaitra à propos de votre livre sera enregistré dans ce répertoire caché, <em>isolé</em> des fichiers de votre projet.</p>
      <p>Supposons que nous avons déja écrits deux fichiers pour notre livre: <code>remerciements.txt</code>, et <code>résumé.txt</code
      .>.</p>
      <p>Il est maintenant temps de donner à Git un (ou plusieurs) fichiers à versionner. Vous devrez explicitement indiquer à Git quel est le contenu de chaque point de contrôle (en language Git, un : <em>commit</em>). Pour le moment nous n'allons ajouter que les remerciements <code>git add remerciements.txt</code>. C'est ce qu'on appelle <em>staging</em> le fichier <code>remerciements.txt</code>: ce qui revient à le taguer comme devant être inclu dans le prochain commit.</p>
    </div>;
  }
});

module.exports = Init;
