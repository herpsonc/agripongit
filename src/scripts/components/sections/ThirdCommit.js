/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var ThirdCommit = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <Highlight className="bash">git commit</Highlight>
      <p>Ce nouveau commit s'ajoute sous sur le précédent commit associé au label <code>HEAD</code> et le pointeur est mis à jour de façaon à ce que <code>HEAD</code> pointe sur le dernier commit.</p>
      <p>Interessons maintenant au label intitulé <code>master</code>. C'est ce que l'on appelle une <em>branche</em>. Comme <code>HEAD</code>, les branches sont des pointeurs vers un commit particulier. 
      Contrairement à <code>HEAD</code>, vous pouvez par contre en choisir le nom. Vous pouvez d'ailleur disposer de plusieurs branches simultanéement . Commme vous pouvez le voir, la branche par défaut
      se nomme <code>master</code>.</p>
    </div>;
  }
});

module.exports = ThirdCommit;
