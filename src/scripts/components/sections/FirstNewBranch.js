/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var FirstNewBranch = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Nous pouvons facilement créer une nouvelle branche. Appelons la <code>maBranche</code>:</p>
      <Highlight className="bash">git branch maBranche</Highlight>
      <p>Comme vous pouvez le voir,  il y a maintenant 2 labelsqui pointent vers le commit <code>HEAD</code> : <code>master</code> et <code>maBranche</code>.</p>
    </div>;
  }
});

module.exports = FirstNewBranch;
