/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var CommitAfterPush = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>We can simply continue working on our book and create a new commit as usual:</p>
      <Highlight className="bash">git commit</Highlight>
      <p>As long as do not push, our local changes will not be shared with others. While we were making our local changes, however, it is very well possible that our copy editor has proofread our first draft and submitted some corrections to the <code>publisher</code> repository.</p>
    </div>;
  }
});

module.exports = CommitAfterPush;
