/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

// Take a snapshot of the `trees` part of the state object, every time a new section is entered
// (signified by the ENTER_SECTION action), and save it to the `snapshots` part of the state object.
// That way, we have an associated state of the tree for every section.

import { VISIT_SECTION } from '../actions/view';
import Immutable from 'immutable';
import trees from './trees';

const snapshot = (state = {}, action) => {
  const snapshots = state.snapshots || Immutable.Map();

  if(action.type !== VISIT_SECTION){
    const history = state.history || [];
    return Object.assign({}, state, {
      history: history.concat(action)
    });
  }

  // Furthermore, if a previous snapshot of this section is already active,
  // we don't need to do anything.
  if(snapshots.get(action.sectionName) === state.trees){
    return state;
  }

  if(!snapshots.has(action.sectionName)){
    // This is the first time visiting this section -- take a snapshot.
    return Object.assign({}, state, {
      snapshots: snapshots.set(action.sectionName, state.history)
    });
  }

  // Otherwise we're revisiting a section -- restore its state.
  return Object.assign({}, state, {
    trees: snapshots.get(action.sectionName).reduce(trees, {})
  });
};

export default snapshot;
