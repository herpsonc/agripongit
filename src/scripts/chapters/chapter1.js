/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

import { bindActionCreators } from 'redux';
import * as TreesActionCreators from '../actions/trees';

export default function(dispatch){
  const gitActions = bindActionCreators(TreesActionCreators, dispatch);
  return [
    {
      sectionName: 'init',
      actions: [gitActions.gitInit]
    },
    {
      sectionName: 'firstCommit',
      actions: [gitActions.gitCommit]
    },
    {
      sectionName: 'secondCommit',
      actions: [gitActions.gitCommit]
    },
    {
      sectionName: 'thirdCommit',
      actions: [gitActions.gitCommit]
    },
    {
      sectionName: 'firstNewBranch',
      actions: [() => gitActions.gitBranch('myBranch')]
    },
    {
      sectionName: 'commitToOldBranch',
      actions: [gitActions.gitCommit]
    },
    {
      sectionName: 'switchToNewBranch',
      actions: [() => gitActions.gitCheckout('myBranch')]
    },
    {
      sectionName: 'commitToNewBranch',
      actions: [gitActions.gitCommit]
    },
    {
      sectionName: 'continueOnNewBranch',
      actions: [gitActions.gitCommit]
    },
    {
      sectionName: 'mergeMasterToMyBranch',
      actions: [() => gitActions.gitMerge('master')]
    },
    {
      sectionName: 'switchBackToMaster',
      actions: [() => gitActions.gitCheckout('master')]
    },
    {
      sectionName: 'mergeMyBranchBackToMaster',
      actions: [() => gitActions.gitMerge('myBranch')]
    },
    {
      sectionName: 'addRemote',
      actions: [() => gitActions.gitRemoteAdd('publisher')]
    },
    {
      sectionName: 'push',
      actions: [() => gitActions.gitPush('publisher', 'master')]
    },
    {
      sectionName: 'commitAfterPush',
      actions: [gitActions.gitCommit]
    },
    {
      sectionName: 'fetch',
      actions: [() => gitActions.gitFetch('publisher')]
    },
    {
      sectionName: 'mergeRemote',
      actions: [() => gitActions.gitMerge('publisher/master')]
    },
    {
      sectionName: 'pushMerge',
      actions: [() => gitActions.gitPush('publisher', 'master')]
    }
  ];
};
