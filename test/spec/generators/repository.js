'use strict';

import Immutable from 'immutable';
import qc from 'quick_check';

const generateCommitHash = () => {
  // qc can generate empty strings, but we don't want that for commit hashes
  return 'a' + qc.intUpto(999999999999);
};

const pickOne = (arr) => {
  return qc.array.subsetOf(arr, {length: 1})()[0];
};

// TODO Allow generation of merge commits
const generateTree = (parentHash, tree, nrOfCommits) => {
  if(nrOfCommits === 0){
    return tree;
  }
  const childHashes = qc.arrayOf(generateCommitHash, { length: Math.max(1, qc.intUpto(nrOfCommits)) })();

  let offSpringPerChild = [],
      nrOfCommitsLeft = nrOfCommits - childHashes.length;
  for(let i = 0; i < (childHashes.length - 1); i++){
    const offSpringForThisChild = qc.intUpto(nrOfCommitsLeft);
    nrOfCommitsLeft = nrOfCommitsLeft - offSpringForThisChild;
    offSpringPerChild.push(offSpringForThisChild);
  }
  offSpringPerChild.push(nrOfCommitsLeft);

  childHashes.forEach((childHash, index) => {
    let childCommit = {
      message: 'Commit message',
      parents: [parentHash],
      children: []
    };
    tree = tree.set(childHash, childCommit);

    tree = generateTree(childHash, tree, offSpringPerChild[index]);
  });

  // Add the children to the parent
  tree = tree.update(parentHash, (parentCommit) => {
    parentCommit.children = childHashes;
    return parentCommit;
  });

  return tree;
};

const generateRepository = () => {
  return (nrOfCommits) => {
    const rootHash = generateCommitHash();
    let rootCommit = {
      message: 'Commit message',
      parents: [],
      children: []
    };

    const tree = generateTree(rootHash, Immutable.Map([[rootHash, rootCommit]]), nrOfCommits);
    const head = pickOne(Object.keys(tree.toObject()));
    // Random amount of branches pointing to random commits
    // const branches = Immutable.Map(
    //   qc.objectOf(
    //     pickOne(Object.keys(tree.toObject()))
    //   )(qc.intUpto(nrOfCommits))
    // ).set('master', head);
    const branches = Immutable.Map({master: head});
    const currentBranch = pickOne(Object.keys(branches.toObject()));
    return {
      tree: tree,
      root: rootHash,
      head,
      branches,
      currentBranch
    }
  };
};

export default generateRepository;
