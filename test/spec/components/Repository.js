'use strict';

import Immutable from 'immutable';
import qc from 'quick_check';
import generateRepository from '../generators/repository';

describe('Repository component', function () {
  var React = require('react');
  var Repository = require('../../../src/scripts/components/Repository.js');
  var component;

  describe(', when calculating the tree\'s layout', function(){
    it('should not alter the layout map when there is just one commit', function() {
      expect(function(repo) {
          const instance = new Repository(repo);
          let initialLayoutObject = {};
          initialLayoutObject[repo.root] = {
            position: { x: 50, y: 10 },
            horizontalRange: { from: 0, to: 100 }
          };
          const initialLayout = Immutable.Map(initialLayoutObject);
          return repo.tree.get(repo.root).children.length > 0 ||
                 instance.calculateLayout(repo.root, repo.tree.get(repo.root).children, initialLayout) === initialLayout;
      }).forAll(generateRepository());
    });

    it('should position every first child on the left and every last child on the right', function() {
      expect(function(repo) {
          const instance = new Repository(repo);
          let initialLayoutObject = {};
          initialLayoutObject[repo.root] = {
            position: { x: 50, y: 10 },
            horizontalRange: { from: 0, to: 100 }
          };
          const initialLayout = Immutable.Map(initialLayoutObject);
          const layout = instance.calculateLayout(repo.root, repo.tree.get(repo.root).children, initialLayout);
          return repo.tree.toArray().reduce(function(allValid, commit){
            if(commit.children.length >= 2){
              return allValid
                     && layout.get(commit.children[0]).labelPosition === 'left'
                     && layout.get(commit.children[commit.children.length - 1])
                        .labelPosition === 'right';
            }
            return true;
          }, true);
      }).forAll(generateRepository());
    });
  });
});
