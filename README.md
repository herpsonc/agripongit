# To get set up
With `npm` installed, run `npm install`.

# To try it out
Run `grunt test` to check whether the tests still succeed, and `grunt serve` to start a local server to check whether everything looks as expected.

# To build
`grunt build`

# To deploy
If you're Vincent, you can deploy to Amazon S3 using the following command:
    aws s3 sync ./dist/ s3://agripongit.vincenttunru.com --profile=agripongit
